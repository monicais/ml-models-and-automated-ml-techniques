# ML Models and Automated ML Techniques
## _We use the MIMIC-III Database_

This project aims to utilize various ML techniques to predict the ICU patient mortality and evaluate existing automated ML packages for feature engineering and performance of ML models. The techniques and packages are:
- ANN
- Bagging
- Decision Tree
- Extreme Gradient Boosting (XGBoost)
- K-Nearest Neighbour (KNN)
- Linear Discriminant
- Logistic Regression
- Random Forest
- Autofeat
- H2O AutoML framework

## Frameworks 

<img src = "images/keras.png" width=100, height=100>
<img src = "images/logo-H20.png" width=60, height=60>



Autofeat: https://github.com/cod3licious/autofeat


## Eligible criteria
- Age range from 18 to 90
- Mechanical ventilation

## Information related to the final dataset
- 67 predictive features
- 18,883 distinct patients

## Tests to identify issues related to data
- T-test
- Chi-square goodness of fit
- D'Agostino K-square test
- Mann-Whitney test
- Median imputation for missing values
- Kullback-Leibler (KL) divergence

## Sampling technique
- Random for under sampling
- Synthetic Minority Oversampling Technique (SMOTE)

## The blocks of the project
<p align="center">
<img src = "images/Appendix_A2_end_to_end_flowchart.png" >
</p>

## Parameters and Hyperparameters to consider
- Number of trees depth
- Degree of the polynomial
- Number of layers 
- Size of layers
- Activation function
- The criterion to optimize GridSearch is Recall.

## Accuracy Metrics
- Precision and Recall
- F1 score
- Sensitivity and Specificity
- ROC and AUC
- Calibration curve
- Overall accuracy

## Results without dimension reduction (without Autofeat)
- 46 models in the case of the imbalanced testing dataset
- 46 models in the case of different testing datasets (imbalanced testing set, under sampling testing set, and SMOTE testing set)
- Tuned XGBoost gives a death recall of 0.83, accuracy of 81 and AUC score of 0.91

## Results with dimension reduction (with Autofeat)
- Autofeat was applied and the results demonstrated a potential benefit in reducing predictive features without significantly compromising model performance.

## Contributors
